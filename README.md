## TP POO
1. Créer un projet PHP tout bien comme y faut en suivant les étapes indiquées dans le README du dépôt php-introduction
2. Une fois lancé et tout, mettre un index.php dans le dossier public avec un ptit echo ou autre pour tester que ça marche bien
3. Créer un dossier src/Cooking avec une classe Ingredient à l'intérieur (avec son namespace qui va bien et tout)
4. La classe Ingredient aura comme propriétés :
    * name: string
    * calories: int
    * vegetal: bool
    * raw: bool
Elle aura également une méthode cook() qui fera passer l'Ingredient de cru à cuit (raw = false)
5. Dans le dossier Cooking, rajouter une nouvelle classe Dish et la faire hériter de la classe Ingredient (l'héritage marche de la même manière qu'en javascript)
6. La classe Dish ressemblera à :
une propriété privée ingredients de type array qui contiendra la liste des ingrédients du plat et une méthode addIngredient qui permettra de rajouter un nouvel ingrédient au plat
7. Dans Dish faire un construct sans paramètre qui aura comme première ligne l'appel au constructeur parent :  `parent::__construct("",0,true,true);`
8. Faire que la méthode addIngredient rajoute un Ingredient à la liste du Dish, et modifie en conséquence la valeur actuelle de la propriété calorie du plat ainsi que si le plat est toujours vegetal ou non selon ce qu'on vient de rajouter comme ingrédient
9. Surcharger la méthode cook pour faire qu'elle cuise tous les ingrédient du Dish en plus du Dish lui même, et modifier les calories en conséquence (avec une boucle foreach)


## TP Formulaire User (projet php-exercice)
1. Créer un dossier Entities avec une classe User à l'intérieur
2. Faire que le User ait comme propriété un username:string, un email:string, une birthdate:\DateTime, un password:string
3. Dans le dossier public, créer un fichier register.html dans lequel on mettra un formulaire html d'inscription avec des inputs correspondant au User (donc un pseudo, un email, un password, une date)
(http://php.net/manual/en/class.datetime.php pour voir comment on se sert de la datetime, ya des exemples dans les commentaires en bas)
4. Faire un fichier add-user.php dans lequel on récupère les informations du formulaire pour créer une instance de User avec et en faire un var_dump
5. Ajouter de la validation dans le add-user.php, on ne créera l'instance que si :
    * Le password fait plus de 3 caractères
    * La date est dans un format correct (chercher sur internet comment on valide une date avec DateTime)
6. Vérifier que l'email est valide en regardant comment s'utilise le FILTER_VALIDATE_EMAIL et en l'utilisant avec un filter_input
