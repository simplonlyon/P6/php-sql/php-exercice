<?php

use App\Cooking\Ingredient;
use App\Cooking\Dish;

require "../vendor/autoload.php";

$banana = new Ingredient("banana", 40, true);

$milkshake = new Dish();

$milkshake->addIngredient($banana);
// echo "<pre>";
// var_dump($milkshake);
// echo "</pre>";

$milkshake->addIngredient(new Ingredient("milk", 50, false));

$bananSplit = new Dish();

$bananSplit->addIngredient($milkshake);
$bananSplit->addIngredient(new Ingredient("ice cream", 75, false));
$bananSplit->cook();